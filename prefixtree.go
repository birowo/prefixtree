package prefixtree

import (
	"errors"
)

type Nd struct {
	Chr,
	Prfx string
	Val int
	Nds [127]*Nd
}

func (nd *Nd) Set(str string, val int) (err error) {
	if str == "" || val < 1 {
		return errors.New("empty str or less than one val")
	}
	strLen := len(str)
	bgn := 0
	for bgn < strLen {
		chr := str[bgn]
		if chr > 126 {
			err = errors.New("invalid char :" + string(chr))
			return
		}
		bgn++
		if nd.Nds[chr] == nil {
			nd.Nds[chr] = &Nd{string(chr), str[bgn:], val, [127]*Nd{}}
			return
		}
		nd = nd.Nds[chr]
		end := bgn + len(nd.Prfx)
		if strLen < end {
			end = strLen
		}
		if nd.Prfx != str[bgn:end] {
			i := 0
			for bgn < end && nd.Prfx[i] == str[bgn] {
				i++
				bgn++
			}
			var nds [127]*Nd
			if nd.Prfx[i] != 0 {
				nds[nd.Prfx[i]] = &Nd{string(nd.Prfx[i]), nd.Prfx[i+1:], nd.Val, nd.Nds}
				nd.Val = 0
			}
			if bgn < strLen {
				nds[str[bgn]] = &Nd{string(str[bgn]), str[bgn+1:], val, [127]*Nd{}}
			} else {
				nd.Val = val
			}
			nd.Prfx = nd.Prfx[:i]
			nd.Nds = nds
			return
		}
		bgn = end
	}
	return
}
func (nd *Nd) Get(str string) int {
	strLen := len(str)
	bgn := 0
	for str[bgn] < 127 {
		nd = nd.Nds[str[bgn]]
		if nd == nil {
			return 0
		}
		bgn++
		end := bgn + len(nd.Prfx)
		if end < strLen && nd.Prfx == str[bgn:end] {
			bgn = end
		} else if nd.Val > 0 && nd.Prfx == str[bgn:] {
			return nd.Val
		} else {
			return 0
		}
	}
	return 0
}
