package main

import (
	"encoding/json"
	"regexp"

	"gitlab.com/birowo/prefixtree"
)

func main() {

	type KV struct {
		k string
		v int
	}
	kvs := []KV{
		{"abcdef", 1},
		{"bcd", 2},
		{"abc", 3},
		{"bcdefg", 4},
		{"acd", 5},
		{"bde", 6},
		{"abcdfg", 7},
		{"bcdegh", 8},
		{"abcdfgh", 9},
		{"bcdeg", 10},
	}
	nd := new(prefixtree.Nd)
	for _, kv := range kvs {
		nd.Set(kv.k, kv.v)
	}
	bs, _ := json.MarshalIndent(nd.Nds, "", "   ")
	re := regexp.MustCompile("null,\\s+")
	bs = re.ReplaceAll(bs, []byte(""))
	println(string(bs))
	println()
	for _, kv := range kvs {
		val := nd.Get(kv.k)
		if val != kv.v {
			println("TEST FAIL", val, "!=", kv.v)
			return
		}
		println("GET", kv.k, ":", val)
	}
	println("TEST PASS")
}
